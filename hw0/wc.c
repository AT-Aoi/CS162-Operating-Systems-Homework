#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>

static int lines = 0, words = 0, bytes = 0;

void countLine(int ch) {
    if (ch == '\n') ++lines;
}

void countWord(int ch, bool *atWord) {
    int nowAtWord = !isspace(ch);
    if (*atWord && !nowAtWord) ++words;
    *atWord = nowAtWord;
}

void countBytes() {
    ++bytes;
}

void wc() {
    int ch;
    bool atWord = true;
    while (ch = getchar(), ch != EOF) {
        countLine(ch);
        countWord(ch, &atWord);
        countBytes();
    }
    if (atWord) ++words;
}

int main(int argc, char *argv[]) {
    if (argc == 2) freopen(argv[1], "r", stdin); 

    wc();
    printf(" %d %d %d %s\n", lines, words, bytes, argv[1]);

    return 0;
}
